# Este site é destinado para você que busca fazer duplo diploma destinado para Alemanha, com a Escola Politécnica 

# Neste site, você encontrará

- Como funciona o processo de Duplo Diploma pela POLI
- Orientações e conselhos
- Informações a respeito da proficiência em alemão

# A história da Alemanha

A história da Alemanha é rica, complexa e profundamente influente no contexto europeu e mundial. Ela remonta a milhares de anos, mas aqui vamos destacar os principais eventos e períodos-chave ao longo de sua história:

Antiguidade: As terras que hoje compõem a Alemanha eram habitadas por diversas tribos germânicas durante a Antiguidade. Os romanos mantiveram contato com essas tribos, incluindo os suevos e os francos.

Sacro Império Romano-Germânico (800-1806): O Sacro Império Romano-Germânico foi estabelecido quando Carlos Magno foi coroado Imperador do Ocidente pelo Papa Leão III em 800. Este império uniu várias regiões da Europa Central sob um único governo, mas manteve uma descentralização considerável.

Reforma Protestante (século XVI): Martinho Lutero, um monge alemão, desencadeou a Reforma Protestante em 1517, contestando a Igreja Católica e dando início a uma profunda divisão religiosa na Alemanha.

Guerra dos Trinta Anos (1618-1648): Este conflito devastador teve origem na Alemanha e resultou em uma grande destruição e na morte de uma grande parte da população.

Unificação Alemã (1871): O chanceler Otto von Bismarck liderou o processo de unificação das várias principados e estados alemães em um único país, o Império Alemão, sob o comando do rei Guilherme I da Prússia.

Primeira e Segunda Guerras Mundiais (1914-1918 e 1939-1945): A Alemanha desempenhou um papel central em ambas as guerras, com a Primeira Guerra resultando na queda da monarquia alemã e na criação da República de Weimar, e a Segunda Guerra Mundial levando à devastação do país e ao subsequente período de ocupação e divisão entre as potências aliadas.

Guerra Fria e Divisão da Alemanha (1949-1990): Após a Segunda Guerra Mundial, a Alemanha foi dividida em Alemanha Ocidental (República Federal da Alemanha) e Alemanha Oriental (República Democrática Alemã), com uma fronteira física e ideológica marcada pela Guerra Fria. A queda do Muro de Berlim em 1989 e a reunificação da Alemanha em 1990 marcaram o fim dessa divisão.

União Europeia e liderança econômica: A Alemanha desempenhou um papel fundamental na formação da União Europeia e tornou-se uma potência econômica líder no continente.

Hoje, a Alemanha é conhecida por sua forte economia, cultura rica, contribuições para a ciência e tecnologia, bem como sua influência política na União Europeia. Sua história é um testemunho de superação de desafios e transformações significativas ao longo dos séculos.

# Universidades de engenharia

As universidades de engenharia na Alemanha têm uma reputação global de excelência e são altamente respeitadas em todo o mundo. Aqui está um breve resumo sobre essas instituições:

Diversidade de Ofertas: A Alemanha abriga uma ampla variedade de universidades de engenharia, desde as instituições de renome mundial até escolas técnicas regionais. Isso oferece uma ampla gama de opções para estudantes interessados em várias disciplinas de engenharia.

Educação de Qualidade: As universidades de engenharia alemãs são conhecidas por oferecer educação de alta qualidade. Seus programas de graduação e pós-graduação são altamente especializados e projetados para preparar os estudantes para carreiras de sucesso na engenharia.

Foco em Pesquisa: Muitas universidades de engenharia na Alemanha têm uma forte ênfase na pesquisa. Elas estão envolvidas em projetos inovadores e colaboram com empresas e instituições de pesquisa, o que proporciona aos estudantes a oportunidade de participar de projetos significativos.

Estreita Ligação com a Indústria: A Alemanha é conhecida por sua indústria forte e inovadora. As universidades de engenharia muitas vezes mantêm parcerias próximas com empresas líderes, proporcionando aos estudantes estágios e oportunidades de networking valiosas.

Acesso a Recursos Avançados: Os estudantes de engenharia na Alemanha têm acesso a laboratórios de ponta, tecnologia de última geração e instalações de pesquisa de alto nível, que são fundamentais para o desenvolvimento de suas habilidades práticas.

Programas em Inglês: Muitas universidades alemãs oferecem programas de engenharia em inglês, tornando o país atraente para estudantes internacionais que desejam estudar em um ambiente acadêmico de alto nível.

Custo Acessível ou Gratuito: A maioria das universidades públicas na Alemanha não cobra mensalidades ou cobra taxas de matrícula muito baixas, tornando o ensino superior acessível para estudantes nacionais e estrangeiros.

Qualidade de Vida: Além da excelente educação, a Alemanha oferece uma alta qualidade de vida, com uma boa infraestrutura, transporte público eficiente e uma variedade de atividades culturais e de lazer.

Algumas das universidades de engenharia mais renomadas na Alemanha incluem o Instituto de Tecnologia de Karlsruhe (KIT), a Universidade Técnica de Darmstadt (TUDa), a Universidade Técnica de Munique (TUM), a Universidade Técnica de Berlim (TU Berlin) e a Universidade de Stuttgart. Essas instituições são conhecidas por seus programas de engenharia de classe mundial e contribuem significativamente para o avanço da ciência e da tecnologia na Alemanha e no mundo.