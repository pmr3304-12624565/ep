+++
title = 'O processo de Duplo Diploma'
date = 2023-09-26T12:25:34-03:00
draft = false
+++

O Processo de Duplo diploma na POLI consiste de 3 etapas:
- primeiro uma filtragem geral por meio da análise das notas daqueles que se inscreveram para o processo. Nessa etapa o principal ponto analisado são as notas, poré uma carta de recomendação de um professor, um projeto profissional e o currículo do aluno também devem ser enviados para análise. 
- Em segundo lugar, é feita uma entrevista com o orgão CRINT que cuida da questão do intercâmbio. Nessa entrevista, alguns professores irão perguntar aos alunos quais são suas perspectivas e ideias para a experiência que viverão no exterior.
- Por último é o envio de documentos e histórico para a univeresidade do exterior que o aluno foi direcionado. 

    Todo esse processo pode ser um pouco cansativo, mas confie em mim, no final vale a pena!!

Além disso, algumas universidades exigem que seja feita uma prova e uma segunda entrevista. Caso você esteja buscando apenas a Alemanha, esse não é seu problema!