+++
title = 'questões de proficiência'
date = 2023-09-26T12:25:34-03:00
draft = false
+++
A respeito do idioma, para participar do programa de duplo diploma na Escola Politécnica com universidades alemãs que possuem convênio, é exigida a proficiência em alemão nos níveis B1 e B2 antes de sua ida para a Alemanha. Essa exigência é fundamental para garantir que você tenha uma experiência acadêmica bem-sucedida e integrada.

Aqui estão algumas maneiras de atingir esses níveis de proficiência em alemão:

Cursos de Idiomas: Inscreva-se em cursos de idiomas em uma escola de idiomas local, em uma universidade ou online. Existem muitos recursos e programas dedicados ao ensino do alemão para estudantes estrangeiros.

Prática Regular: Dedique tempo todos os dias para praticar o alemão. Isso pode incluir ouvir músicas, assistir a filmes ou séries em alemão, ler livros e revistas ou participar de grupos de conversação.

Aplicativos e Recursos Online: Utilize aplicativos e recursos online, como Duolingo, Memrise, Babbel e outros, que são projetados para ajudar a melhorar suas habilidades em alemão.

Tandem Language Partners: Procure por um parceiro de idiomas, um estudante alemão que queira aprender sua língua materna. Isso pode ser uma maneira eficaz de praticar e melhorar suas habilidades conversacionais.

Cursos Universitários: Verifique se sua universidade oferece cursos de alemão para estudantes estrangeiros. Eles geralmente são adaptados para atender às necessidades acadêmicas.

Exames de Proficiência: Considere fazer exames de proficiência reconhecidos, como o TestDaF (Test Deutsch als Fremdsprache) ou o Goethe-Zertifikat. Eles podem ser um indicador útil do seu progresso.

Lembre-se de que a prática constante e a imersão na língua são cruciais para melhorar sua proficiência. Planeje com antecedência e, ao alcançar os níveis exigidos, você estará bem preparado para aproveitar ao máximo sua experiência de duplo diploma na Alemanha.

Segue um curso do YouTube, que me ajudou nessa caminhada:

- [![Irmão Alemão](https://i.ytimg.com/vi/QB9aOPVQ7dw/hq720.jpg?sqp=-oaymwEhCK4FEIIDSFryq4qpAxMIARUAAAAAGAElAADIQj0AgKJD&rs=AOn4CLA4PDSJ5dWPfc-eU39SITGRBznsQA)](https://youtu.be/iMe1P2x5Byw?si=D_DlkZtPc1kj4slA)